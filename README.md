# Mediasoft Blog App

Mediasoft Blog App - серверное приложение блога реализованое на Python с использованием фреймворка Django.

## Версии ПО

### Приложение
- Python v3.11
- Django v4.2.4
- Django Rest Framework v3.14.0
- Drf Spectacular v0.26.4
- Django-filter v23.2
- Django Rest Framework Camel Case v1.4.2
- Django Cors Headers v4.2.0
- Django Rest Framework Simplejwt v5.3.0
- Environs v9.5.0
- Psycopg2-binary v2.9.6
- Gunicorn v21.2.0

### Форматтеры
- Black v23.10.1
- Ruff v0.1.3

### Линтеры
- Ruff v0.1.3

### Контейнеры
- Docker-compose v3.9
- Postgres v14 alpine

## Клонирование проекта

```git
git clone git@gitlab.com:vlad_kuzmin/mediasoft_blog_app.git
```

```shell
 cd mediasoft_blog_app
 ```

## Начало работы

Перед запуском приложения вам необходимо добавить файл виртуального окружения (.env).
Описание этого файла находится в корневом катологе приложения (env.tmpl). Также вам необходимо
добавить файл настроек применяемых в режиме разработки (dev_settings.py), в ту же дирректорию где находится шаблон
(dev_settings.tmpl), если вы планируете запускать приложение в режиме разработки.

После того как виртуальное окружение настроено необходимо собрать и запустить Docker-контейнеры.
Для этого перейдите в корневую директорию и выполните следующие действия.

### Подготовка проекта для разработки

1. Скопируйте файл `env.tmpl` в `.env`
2. Скопируйте файл `src/_project/settings/dev_settings.tmpl` в `src/_project/settings/dev_settings.py`
3. Приступайте к сборке контейнеров

### Сборка контейнеров

```shell
make build
```

### Запуск

```shell
make up
```

Остальные команды можно найти в Makefile проекта.
