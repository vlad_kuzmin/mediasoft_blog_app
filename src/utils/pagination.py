from rest_framework.pagination import PageNumberPagination


class PaginationWithQueryParam(PageNumberPagination):
    """
    Кастомный класс пагинации предоставляющий возможность
    изменять размер страницы через параметр запроса "page_size".
    """

    page_size = 10
    page_size_query_param = "page_size"
    max_page_size = 100
