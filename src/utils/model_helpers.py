from users.models import User


def default_post_user() -> User:
    user = User.objects.get(username="blog_user")
    return user
