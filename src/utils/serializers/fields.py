from rest_framework import serializers


class FilteredPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    """
    Кастомное поле PrimaryKeyRelatedField с возможностью фильтрации queryset'а.
    """

    def __init__(self, **kwargs):
        self.pk_field = kwargs.pop("pk_field", None)
        self.filter_kwargs = kwargs.pop("filter_kwargs", None)
        super(FilteredPrimaryKeyRelatedField, self).__init__(**kwargs)

    def get_queryset(self):
        queryset = super(FilteredPrimaryKeyRelatedField, self).get_queryset()
        if not self.filter_kwargs or not queryset:
            return None
        return queryset.filter(**self.filter_kwargs)
