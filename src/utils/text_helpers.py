from django.utils.text import Truncator


def truncate_words(string: str, length: int) -> str:
    return Truncator(string).words(length, truncate=" …")
