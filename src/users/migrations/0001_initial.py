# Generated by Django 4.2.4 on 2023-08-21 12:33

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("auth", "0012_alter_user_first_name_max_length"),
    ]

    operations = [
        migrations.CreateModel(
            name="User",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "password",
                    models.CharField(max_length=128, verbose_name="password"),
                ),
                (
                    "username",
                    models.CharField(
                        help_text="User's username",
                        max_length=50,
                        unique=True,
                        verbose_name="username",
                    ),
                ),
                (
                    "email",
                    models.CharField(
                        help_text="User's username",
                        max_length=50,
                        unique=True,
                        validators=[django.core.validators.EmailValidator],
                        verbose_name="email",
                    ),
                ),
                (
                    "first_name",
                    models.CharField(
                        help_text="User's first name",
                        max_length=30,
                        verbose_name="first name",
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        help_text="User's last name",
                        max_length=40,
                        verbose_name="last name",
                    ),
                ),
                (
                    "birthday",
                    models.DateField(
                        blank=True, null=True, verbose_name="birthday date"
                    ),
                ),
                (
                    "avatar",
                    models.ImageField(
                        blank=True,
                        help_text="User's avatar image",
                        null=True,
                        upload_to="users/avatars/",
                        verbose_name="user's avatar",
                    ),
                ),
                (
                    "date_registration",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="date of registry user"
                    ),
                ),
                (
                    "last_login",
                    models.DateTimeField(
                        auto_now=True, verbose_name="date of last login user"
                    ),
                ),
                (
                    "is_superuser",
                    models.BooleanField(
                        default=False,
                        help_text="Designates that this user has all permissions without explicitly assigning them.",
                        verbose_name="superuser status",
                    ),
                ),
                (
                    "is_staff",
                    models.BooleanField(
                        default=False,
                        help_text="Designates whether the user can log into this admin site.",
                        verbose_name="staff status",
                    ),
                ),
                (
                    "is_active",
                    models.BooleanField(
                        default=True,
                        help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts.",
                        verbose_name="active status",
                    ),
                ),
                (
                    "groups",
                    models.ManyToManyField(
                        blank=True,
                        help_text="The groups this user belongs to. A user will get all permissions granted to each of their groups.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.group",
                        verbose_name="groups",
                    ),
                ),
                (
                    "user_permissions",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Specific permissions for this user.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.permission",
                        verbose_name="user permissions",
                    ),
                ),
            ],
            options={
                "verbose_name": "user",
                "verbose_name_plural": "users",
                "db_table": "users",
                "ordering": ("id",),
            },
        ),
    ]
