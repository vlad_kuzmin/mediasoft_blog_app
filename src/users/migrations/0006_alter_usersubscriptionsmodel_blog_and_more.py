# Generated by Django 4.2.4 on 2023-11-14 17:36

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        (
            "blog",
            "0005_remove_postmodel_is_published_postmodel_status_and_more",
        ),
        ("users", "0005_alter_usersubscriptionsmodel_options_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="usersubscriptionsmodel",
            name="blog",
            field=models.ForeignKey(
                help_text="subscribed blog",
                on_delete=django.db.models.deletion.CASCADE,
                related_name="blog_subscribers",
                to="blog.blogmodel",
                verbose_name="blog",
            ),
        ),
        migrations.AlterField(
            model_name="usersubscriptionsmodel",
            name="user",
            field=models.ForeignKey(
                help_text="subscribing user",
                on_delete=django.db.models.deletion.CASCADE,
                related_name="subscriptions",
                to=settings.AUTH_USER_MODEL,
                verbose_name="user",
            ),
        ),
    ]
