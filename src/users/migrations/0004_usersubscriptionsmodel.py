# Generated by Django 4.2.4 on 2023-11-09 21:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("blog", "0003_commentmodel"),
        ("users", "0003_remove_user_avatar_remove_user_is_staff_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="UserSubscriptionsModel",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "blog",
                    models.ForeignKey(
                        help_text="subscribed blog",
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="blog_subscriptions",
                        to="blog.blogmodel",
                        verbose_name="blog",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        help_text="subscribing user",
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="user_subscriptions",
                        to=settings.AUTH_USER_MODEL,
                        verbose_name="user",
                    ),
                ),
            ],
            options={
                "verbose_name": "user subscriptions",
            },
        ),
    ]
