from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    """
    Создаёт 1-го пользователя и 1-го суперпользователя если таковых не существует.
    Используется при инициализации дефолтных пользователей проекта.
    """

    help = "Creates the 1st user and 1st super user of the project if they do not exist"

    def create_default_user(self):
        """
        Создает стандартного пользователя без прав администратора.
        """
        try:
            user = User.objects.get(username="blog_user")
            self.stdout.write(
                self.style.SUCCESS(f"User {user.username} already created"),
                ending="\n",
            )
        except User.DoesNotExist:
            user = User.objects.create_user(
                username="blog_user",
                email="blog_user@yandex.ru",
                password="yV6{lG9)",
            )
            if user:
                self.stdout.write(
                    self.style.SUCCESS(
                        f"User {user.username} successfully created"
                    ),
                    ending="\n",
                )
            else:
                self.stdout.write(
                    self.style.ERROR("The standard user was not created!")
                )

    def create_default_superuser(self):
        """
        Создает стандартного пользователя с правами администратора и суперпользователя.
        """
        try:
            superuser = User.objects.get(username="admin")
            self.stdout.write(
                self.style.SUCCESS(
                    f"Superuser {superuser.username} already created"
                ),
                ending="\n",
            )
        except User.DoesNotExist:
            superuser = User.objects.create_superuser(
                username="admin",
                email="admin@yandex.ru",
                password="hL4*pZ2'nP4;",
            )
            if superuser:
                self.stdout.write(
                    self.style.SUCCESS(
                        f"Superuser {superuser.username} successfully created"
                    ),
                    ending="\n",
                )
            else:
                self.stdout.write(
                    self.style.ERROR("The standard superuser was not created!")
                )

    def handle(self, *args, **options):
        self.create_default_user()
        self.create_default_superuser()
