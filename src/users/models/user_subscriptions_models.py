from django.db import models
from django.db.models import UniqueConstraint
from django.utils.translation import gettext_lazy as _


class UserSubscriptionsModel(models.Model):
    """
    Модель подписок пользователя.
    """

    user = models.ForeignKey(
        "users.User",
        verbose_name=_("user"),
        on_delete=models.CASCADE,
        help_text=_("subscribing user"),
        related_name="subscriptions",
    )

    blog = models.ForeignKey(
        "blog.BlogModel",
        verbose_name=_("blog"),
        on_delete=models.CASCADE,
        help_text=_("subscribed blog"),
        related_name="blog_subscribers",
    )

    def __str__(self):
        return f"{self.user.username} {self.blog.title}"

    class Meta:
        verbose_name = _("user subscriptions")
        ordering = ("-id",)
        constraints = (
            UniqueConstraint(
                fields=("user", "blog"), name="subscription_unique"
            ),
        )
