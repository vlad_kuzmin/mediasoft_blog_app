from django.db import models
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.password_validation import validate_password
from django.utils.translation import gettext_lazy as _
from django.core.validators import EmailValidator, ValidationError


class UserManager(BaseUserManager):
    """
    Переопределенный менеджер для пользователя.
    """

    user_in_migrations = True
    _db = "users"

    def _create_user(self, username, email, password=None, **extra_fields):
        """
        Приватный метод создания пользователя.
        """

        email = self.normalize_email(email)

        if password is not None:
            try:
                validate_password(password)
            except ValidationError:
                raise ValueError(
                    {
                        "password": _(
                            "Password can't be set. Set stronger password and try again."
                        )
                    }
                )
        else:
            raise ValueError(
                {
                    "password": _(
                        "Password can't be empty. Please set password."
                    )
                }
            )

        user = self.model(username=username, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, username, email, password=None, **extra_fields):
        """
        Создание обычного пользователя.
        """

        extra_fields.setdefault("is_admin", False)
        extra_fields.setdefault("is_superuser", False)
        extra_fields.setdefault("is_active", True)

        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email, password=None, **extra_fields):
        """
        Создание супер-пользователя.
        """

        extra_fields.setdefault("is_admin", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        return self._create_user(username, email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    Переопределенный пользователь в системе.
    """

    username = models.CharField(
        _("username"),
        max_length=50,
        unique=True,
        help_text=_("User's username"),
    )

    email = models.CharField(
        _("email"),
        max_length=50,
        unique=True,
        validators=(EmailValidator,),
        help_text=_("User's email"),
    )

    first_name = models.CharField(
        _("first name"),
        help_text=_("User's first name"),
        max_length=30,
        blank=True,
        null=True,
    )

    last_name = models.CharField(
        _("last name"),
        help_text=_("User's last name"),
        max_length=40,
        blank=True,
        null=True,
    )

    birthday = models.DateField(
        _("birthday date"),
        blank=True,
        null=True,
    )

    date_registration = models.DateTimeField(
        _("date of registry user"), auto_now_add=True
    )

    last_login = models.DateTimeField(
        _("date of last login user"), auto_now=True
    )

    is_superuser = models.BooleanField(
        _("superuser status"),
        default=False,
        help_text=_(
            "Designates that this user has all permissions without "
            "explicitly assigning them."
        ),
    )

    is_admin = models.BooleanField(
        _("admin status"),
        default=False,
        help_text=_("Can create, delete and edit any entities."),
    )

    is_active = models.BooleanField(
        _("active status"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin

    @property
    def is_staff(self):
        return self.is_admin

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email"]
    objects = UserManager()

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
        ordering = ("id",)
        db_table = "users"
