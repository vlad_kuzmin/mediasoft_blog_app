from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsAdminUser(BasePermission):
    """
    Предоставляет доступ только администратору.
    """

    def has_permission(self, request, view):
        return getattr(request.user, "is_admin", False)


class IsAdminUserOrReadOnly(BasePermission):
    """
    Пользователь является администратором или read-only запросы.
    """

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS
            or getattr(request.user, "is_admin", False)
        )

    def has_object_permission(self, request, view, obj):
        if bool(
            not getattr(request.user, "is_admin", False)
            and request.method not in SAFE_METHODS
        ):
            raise PermissionDenied

        return bool(
            request.method in SAFE_METHODS
            or getattr(request.user, "is_admin", False)
        )


class IsOwner(BasePermission):
    """
    Предоставляет доступ только владельцу.
    """

    def has_permission(self, request, view):
        return getattr(request.user, "is_authenticated", False)

    def has_object_permission(self, request, view, obj):
        if not getattr(request.user, "is_authenticated", False):
            raise PermissionDenied

        return obj.owner == request.user


class IsOwnerOrReadOnly(BasePermission):
    """
    Пользователь является владельцем или read-only запросы.
    """

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS
            or getattr(request.user, "is_authenticated", False)
        )

    def has_object_permission(self, request, view, obj):
        if bool(
            not getattr(request.user, "is_authenticated", False)
            and request.method not in SAFE_METHODS
        ):
            raise PermissionDenied

        return bool(
            request.method in SAFE_METHODS or obj.owner == request.user
        )


class IsAuthor(BasePermission):
    """
    Предоставляет доступ только автору.
    """

    def has_permission(self, request, view):
        return getattr(request.user, "is_authenticated", False)

    def has_object_permission(self, request, view, obj):
        if not getattr(request.user, "is_authenticated", False):
            raise PermissionDenied

        return obj.author == request.user


class IsAuthorOrReadOnly(BasePermission):
    """
    Пользователь является автором или read-only запросы.
    """

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS
            or getattr(request.user, "is_authenticated", False)
        )

    def has_object_permission(self, request, view, obj):
        if bool(
            not getattr(request.user, "is_authenticated", False)
            and request.method not in SAFE_METHODS
        ):
            raise PermissionDenied

        return bool(
            request.method in SAFE_METHODS or obj.author == request.user
        )


class IsAccountOwner(BasePermission):
    """
    Предоставляет доступ только владельцу аккаунта.
    """

    def has_permission(self, request, view):
        return getattr(request.user, "is_authenticated", False)

    def has_object_permission(self, request, view, obj):
        if not getattr(request.user, "is_authenticated", False):
            raise PermissionDenied

        return obj == request.user


class IsAccountOwnerOrReadOnly(BasePermission):
    """
    Пользователь является владельцем или read-only запросы.
    """

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS
            or getattr(request.user, "is_authenticated", False)
        )

    def has_object_permission(self, request, view, obj):
        if bool(
            not getattr(request.user, "is_authenticated", False)
            and request.method not in SAFE_METHODS
        ):
            raise PermissionDenied

        return bool(request.method in SAFE_METHODS or obj == request.user)


class IsSubscriptionOwner(BasePermission):
    """
    Предоставляет доступ только владельцу подписки.
    """

    def has_permission(self, request, view):
        return getattr(request.user, "is_authenticated", False)

    def has_object_permission(self, request, view, obj):
        if not getattr(request.user, "is_authenticated", False):
            raise PermissionDenied

        return obj.user == request.user
