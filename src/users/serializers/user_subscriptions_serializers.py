from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from blog.models import PostModel, BlogModel
from users.models import UserSubscriptionsModel

User = get_user_model()


class UserSubscriptionsSerializerMixin:
    """
    Примесь с логикой создания пользовательских подписок.
    """

    def create(self, validated_data):
        """
        Метод создания новой подписки.
        """

        instance = UserSubscriptionsModel()
        current_user = self.context.get("request").user
        instance.user = current_user
        instance.blog = validated_data.pop("blog", None)
        instance.save()

        return instance


class SimpleUserSubscriptionsSerializer(
    UserSubscriptionsSerializerMixin, serializers.ModelSerializer
):
    """
    Сериализует данные модели UserSubscriptionsModel.
    Используется в качестве базового сериализатора
    для создания и обновления данных подписок пользователей.
    """

    user = serializers.SlugRelatedField(
        slug_field="username",
        read_only=True,
        default=serializers.CurrentUserDefault(),
    )
    blog = serializers.PrimaryKeyRelatedField(
        queryset=BlogModel.objects.all(),
        required=True,
    )

    class Meta:
        model = PostModel
        fields = (
            "id",
            "user",
            "blog",
        )
        read_only_fields = (
            "id",
            "user",
        )
        validators = (
            UniqueTogetherValidator(
                queryset=UserSubscriptionsModel.objects.all(),
                fields=("user", "blog"),
            ),
        )
