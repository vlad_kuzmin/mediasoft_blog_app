import itertools
from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.contrib.auth.password_validation import validate_password
from rest_framework.validators import UniqueValidator

User = get_user_model()


class UserSerializerMixin:
    """
    Примесь с логикой создания и обновления пользователей.
    """

    def create(self, validated_data):
        """
        Метод создания нового пользователя.
        """

        email = validated_data.pop("email", None)
        password = validated_data.pop("password", None)

        username = validated_data.pop("username", None)
        is_admin = validated_data.pop("is_admin", False)
        is_active = validated_data.pop("is_active", True)
        is_superuser = validated_data.pop("is_superuser", False)

        return User.objects.create_user(
            username=username,
            email=email,
            password=password,
            is_admin=is_admin,
            is_superuser=is_superuser,
            is_active=is_active,
            **validated_data,
        )

    def update(self, instance, validated_data):
        """
        Метод обновления пользователя.
        """

        instance.email = validated_data.get("email", instance.email)
        instance.password = validated_data.get("password", instance.password)
        instance.first_name = validated_data.get(
            "first_name", instance.first_name
        )
        instance.last_name = validated_data.get(
            "last_name", instance.last_name
        )
        instance.birthday = validated_data.get("birthday", instance.birthday)

        instance.username = validated_data.get("username", instance.username)
        instance.is_admin = validated_data.get("is_admin", instance.is_admin)
        instance.is_active = validated_data.get(
            "is_active", instance.is_active
        )
        instance.is_superuser = validated_data.get(
            "is_superuser", instance.is_superuser
        )

        instance.save()

        return instance


class SimpleUserSerializer(UserSerializerMixin, serializers.ModelSerializer):
    """
    Сериализует данные модели User.
    Используется для получения данных одного пользователя,
    обновления и создания нового пользователя в системе.
    """

    def __init__(self, *args, **kwargs):
        """
        Динамически меняем поля только для чтения при инициализации класса
        в зависимости от статуса пользователя.
        """

        super(SimpleUserSerializer, self).__init__(*args, **kwargs)

        user = self.context.get("request.user", {})
        initial_read_only_fields = getattr(self.Meta, "read_only_fields", ())

        user_read_only_fields = ("username", "is_superuser", "is_admin")
        admin_read_only_fields = ("is_superuser",)

        is_admin_user = getattr(user, "is_admin", False)
        is_superuser = getattr(user, "is_superuser", False)

        read_only_fields = set(initial_read_only_fields)

        if is_admin_user and not is_superuser:
            read_only_fields = set(
                itertools.chain(
                    initial_read_only_fields, admin_read_only_fields
                )
            )
        elif not is_admin_user and not is_superuser:
            read_only_fields = set(
                itertools.chain(
                    initial_read_only_fields, user_read_only_fields
                )
            )

        setattr(self.Meta, "read_only_fields", tuple(read_only_fields))

    password = serializers.CharField(
        max_length=30,
        write_only=True,
        validators=(validate_password,),
    )

    username = serializers.CharField(
        max_length=50,
        validators=(UniqueValidator,),
    )

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "email",
            "password",
            "first_name",
            "last_name",
            "birthday",
            "date_registration",
            "last_login",
            "is_superuser",
            "is_admin",
            "is_active",
        )
        read_only_fields = (
            "id",
            "date_registration",
            "last_login",
        )
