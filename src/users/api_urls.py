from django.urls import path
from users.views import (
    UserListCreateView,
    UserDetailUpdateDeleteView,
    ProfileDetailUpdateDeleteView,
    UserSubscriptionsListCreateView,
    UserSubscriptionsDestroyView,
)

urlpatterns = [
    # Получить список пользователей или создать нового в зависимости от типа запроса (GET/POST).
    path("", UserListCreateView.as_view(), name="user_list_create"),
    # Получить изменить или удалить данные конкретного пользователя в зависимости от запроса (GET/PATCH/DELETE).
    path(
        "<int:user_id>",
        UserDetailUpdateDeleteView.as_view(),
        name="user_detail_update_delete",
    ),
    # Получить изменить или удалить профиль текущего пользователя в зависимости от запроса (GET/PATCH/DELETE).
    path(
        "profile/",
        ProfileDetailUpdateDeleteView.as_view(),
        name="profile_detail_update_delete",
    ),
    # Получить список подписок текущего пользователя или создать новую в зависимости от типа запроса (GET/POST).
    path(
        "my_subscriptions/",
        UserSubscriptionsListCreateView.as_view(),
        name="current_user_subscriptions",
    ),
    # Отписаться от блога.
    path(
        "unsubscribe/<int:blog_id>",
        UserSubscriptionsDestroyView.as_view(),
        name="current_user_delete_subscription",
    ),
]
