from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from users.models import User, UserSubscriptionsModel
from django.utils.translation import gettext_lazy as _


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    """
    Регистрация модели переопределенного пользователя в админ-панели.
    """

    readonly_fields = ("last_login", "date_registration")

    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            _("Personal info"),
            {
                "fields": (
                    "email",
                    "first_name",
                    "last_name",
                    "birthday",
                ),
            },
        ),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_admin",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            _("Important dates"),
            {"fields": ("last_login", "date_registration")},
        ),
    )

    add_fieldsets = (
        (
            _("Personal info"),
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "username",
                    "password1",
                    "password2",
                    "first_name",
                    "last_name",
                    "birthday",
                ),
            },
        ),
        (_("Permissions"), {"fields": ("is_superuser", "is_admin")}),
    )

    list_display = ("username", "email", "first_name", "last_name", "is_admin")
    list_filter = ("is_admin", "is_superuser", "is_active", "groups")


@admin.register(UserSubscriptionsModel)
class UserSubscriptionsAdmin(admin.ModelAdmin):
    """
    Регистрация модели UserSubscriptions в админ-панели.
    """

    list_display = ("user", "blog")
    list_display_links = ("user", "blog")
    list_filter = ("user__is_active", "blog__updated_at")
    search_fields = ("user__username", "blog__title")
