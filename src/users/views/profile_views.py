from django.contrib.auth import get_user_model
from drf_spectacular.utils import extend_schema
from rest_framework import generics, status
from rest_framework.mixins import UpdateModelMixin
from rest_framework.response import Response

from users.permissions import IsAccountOwner
from users.serializers import SimpleUserSerializer

User = get_user_model()


class ProfileDetailUpdateDeleteView(UpdateModelMixin, generics.GenericAPIView):
    """
    Получить, изменить или удалить профиль текущего пользователя.
    """

    permission_classes = (IsAccountOwner,)
    serializer_class = SimpleUserSerializer

    def get_object(self):
        """
        Переопределенный метод получения объекта.
        Возвращает объект пользователя найденный по id пользователя из запроса.
        """
        user = User.objects.get(id=self.request.user.id)
        self.check_object_permissions(self.request, user)

        return user

    def deactivate_account(self, request, *args, **kwargs):
        """
        Деактивация аккаунта пользователя путем обновления поля 'is_active'.
        """
        instance = self.get_object()
        instance.is_active = False
        instance.save(update_fields=("is_active",))
        return Response(status=status.HTTP_200_OK)

    @extend_schema(
        summary="User profile detail view",
        description="Получить профиль текущего пользователя",
    )
    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(instance=request.user)
        if serializer.data:
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": ""}, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(
        summary="User profile update view",
        description="Обновить профиль текущего пользователя",
    )
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @extend_schema(
        summary="User profile delete view",
        description="Удалить текущего пользователя из системы.",
    )
    def delete(self, request, *args, **kwargs):
        return self.deactivate_account(request, *args, **kwargs)
