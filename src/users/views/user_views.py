from django.contrib.auth import get_user_model
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.generics import (
    ListCreateAPIView,
)
from rest_framework.response import Response
from users.serializers import SimpleUserSerializer
from users.permissions import IsAdminUserOrReadOnly
from utils.views.generics import RetrieveUpdateDestroyAPIView

User = get_user_model()


class UserListCreateView(ListCreateAPIView):
    """
    Получить список пользователей или создать нового пользователя в системе.
    """

    permission_classes = (IsAdminUserOrReadOnly,)
    serializer_class = SimpleUserSerializer

    def get_queryset(self):
        """
        Для пользователей без прав только активные аккаунты.
        """
        if self.request.user.is_admin or self.request.user.is_superuser:
            return User.objects.all()
        return User.objects.exclude(is_active=False)

    @extend_schema(
        summary="List users view",
        description="Получить список пользователей в системе",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @extend_schema(
        summary="Create user view",
        description="Создать нового пользователя в системе",
    )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class UserDetailUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    """
    Получить, удалить или изменить данные конкретного пользователя по id.
    """

    permission_classes = (IsAdminUserOrReadOnly,)
    lookup_field = "pk"
    lookup_url_kwarg = "user_id"
    serializer_class = SimpleUserSerializer

    def get_queryset(self):
        """
        Для пользователей без прав только активные аккаунты.
        """
        if self.request.user.is_admin or self.request.user.is_superuser:
            return User.objects.all()
        return User.objects.exclude(is_active=False)

    def deactivate_account(self, request, *args, **kwargs):
        """
        Деактивация аккаунта пользователя путем обновления поля 'is_active'.
        """
        instance = self.get_object()
        instance.is_active = False
        instance.save(update_fields=("is_active",))
        return Response(status=status.HTTP_200_OK)

    @extend_schema(
        summary="User detail view",
        description="Получить данные конкретного пользователя",
    )
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @extend_schema(
        summary="User patch update view",
        description="Изменить данные конкретного пользователя",
    )
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @extend_schema(
        summary="User delete view",
        description="Удалить конкретного пользователя из системы по id",
    )
    def delete(self, request, *args, **kwargs):
        return self.deactivate_account(request, *args, **kwargs)
