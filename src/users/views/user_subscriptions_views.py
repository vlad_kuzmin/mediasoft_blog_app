from drf_spectacular.utils import extend_schema
from rest_framework.generics import ListCreateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from users.models import UserSubscriptionsModel
from users.permissions import IsSubscriptionOwner
from users.serializers import SimpleUserSubscriptionsSerializer


class UserSubscriptionsListCreateView(ListCreateAPIView):
    """
    Получить список подписок текущего пользователя или создать новую.
    """

    serializer_class = SimpleUserSubscriptionsSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        current_user = getattr(self.request, "user", None)
        queryset = UserSubscriptionsModel.objects.filter(user=current_user)

        return queryset

    @extend_schema(
        summary="List subscriptions view",
        description="Получить список подписок текущего пользователя",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @extend_schema(
        summary="Create subscription view",
        description="Создать новую подписку (подписаться)",
    )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class UserSubscriptionsDestroyView(DestroyAPIView):
    """
    Удалить подписку (отписаться).
    """

    serializer_class = SimpleUserSubscriptionsSerializer
    permission_classes = (IsSubscriptionOwner,)
    lookup_field = "blog__pk"
    lookup_url_kwarg = "blog_id"

    def get_queryset(self):
        current_user = getattr(self.request, "user", None)
        queryset = UserSubscriptionsModel.objects.filter(user=current_user)

        return queryset

    @extend_schema(
        summary="Delete subscription view",
        description="Удалить подписку (отписаться)",
    )
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
