from django.contrib.auth import get_user_model
from drf_spectacular.utils import extend_schema
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from users.serializers import LogoutSerializer


User = get_user_model()


class LogoutView(generics.GenericAPIView):
    """
    Выход пользователя из системы с блокировкой refresh токена.
    """

    permission_classes = [IsAuthenticated]
    serializer_class = LogoutSerializer

    @extend_schema(
        summary="Logout view",
        description="Выход пользователя из системы с блокировкой refresh токена",
    )
    def post(self, request, *args):
        sz = self.get_serializer(data=request.data)
        sz.is_valid(raise_exception=True)
        sz.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
