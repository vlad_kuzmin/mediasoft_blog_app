from django.contrib import admin
from django.urls import path, include
from _project.settings import DEBUG, MEDIA_URL, MEDIA_ROOT
from django.conf.urls.static import static


urlpatterns = [
    path("admin/", admin.site.urls),
    # API URL-шаблоны
    path("api/", include("_project.api_urls")),
]

if DEBUG:
    urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)
