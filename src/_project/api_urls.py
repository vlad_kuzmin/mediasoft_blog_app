from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from users.views import LogoutView

urlpatterns = [
    # Users API
    path("users/", include("users.api_urls")),
    # Blog API
    path("blog/", include("blog.api_urls")),
    # Pages API
    path("pages/", include("blog.pages_urls")),
    # spectacular
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "docs/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    # Auth views
    path("auth/login/", TokenObtainPairView.as_view(), name="user_login"),
    path("auth/logout/", LogoutView.as_view(), name="user_logout"),
    path(
        "auth/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"
    ),
]
