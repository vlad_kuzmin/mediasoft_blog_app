from django.contrib import admin
from blog.models import BlogModel, PostModel, TagModel, CommentModel


@admin.register(BlogModel)
class BlogAdmin(admin.ModelAdmin):
    """
    Регистрация модели Blog в админ-панели.
    """

    readonly_fields = ("updated_at", "created_at")
    list_filter = ("updated_at", "created_at")
    search_fields = ("owner__username", "authors__username", "title")


@admin.register(PostModel)
class PostAdmin(admin.ModelAdmin):
    """
    Регистрация модели Post в админ-панели.
    """

    list_display = ("title", "author", "status")
    readonly_fields = ("created_at", "likes", "views")
    list_filter = ("status", "created_at", "likes", "views", "tags__name")
    search_fields = ("author__username", "title", "tags__name")


@admin.register(TagModel)
class TagAdmin(admin.ModelAdmin):
    """
    Регистрация модели Tag в админ-панели.
    """

    search_fields = ("name",)


@admin.register(CommentModel)
class CommentAdmin(admin.ModelAdmin):
    """
    Регистрация модели Comment в админ-панели.
    """

    list_display = ("author", "post")
    readonly_fields = ("created_at",)
    list_filter = ("created_at", "post", "author")
    search_fields = ("author__username", "post__title")
