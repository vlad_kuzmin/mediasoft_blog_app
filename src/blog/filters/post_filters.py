from django_filters import rest_framework as filters
from django.utils.translation import gettext_lazy as _
from blog.models import PostModel


class PostFilter(filters.FilterSet):
    """
    Реализует фильтрацию по дате создания от - до, по тегам,
    а так же сортировки по названию, дате создания, по лайкам и просмотрам в прямом и обратном порядке.
    """

    created_at_start = filters.DateTimeFilter(
        field_name="created_at", lookup_expr="gte"
    )
    created_at_end = filters.DateTimeFilter(
        field_name="created_at", lookup_expr="lte"
    )
    tag = filters.CharFilter(field_name="tags__name")

    ordering = filters.OrderingFilter(
        fields=(
            ("created_at", "created_at"),
            ("title", "title"),
            ("likes", "likes"),
            ("views", "views"),
        ),
    )

    class Meta:
        model = PostModel
        fields = (
            "ordering",
            "created_at_start",
            "created_at_end",
            "tag",
        )
        field_labels = {
            "created_at_start": _("Start created date"),
            "created_at_end": _("End created date"),
            "tag": _("Tag name"),
        }
