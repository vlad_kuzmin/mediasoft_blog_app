from django_filters import rest_framework as filters
from django.utils.translation import gettext_lazy as _
from blog.models import BlogModel


class BlogFilter(filters.FilterSet):
    """
    Реализует фильтрацию по дате создания/обновления от - до,
    а так же сортировки по названию, дате создания/обновления в прямом и обратном порядке.
    """

    updated_at_start = filters.DateTimeFilter(
        field_name="updated_at", lookup_expr="gte"
    )
    updated_at_end = filters.DateTimeFilter(
        field_name="updated_at", lookup_expr="lte"
    )
    created_at_start = filters.DateTimeFilter(
        field_name="created_at", lookup_expr="gte"
    )
    created_at_end = filters.DateTimeFilter(
        field_name="created_at", lookup_expr="lte"
    )

    ordering = filters.OrderingFilter(
        fields=(
            ("updated_at", "updated_at"),
            ("created_at", "created_at"),
            ("title", "title"),
        ),
    )

    class Meta:
        model = BlogModel
        fields = (
            "ordering",
            "updated_at_start",
            "updated_at_end",
            "created_at_start",
            "created_at_end",
        )
        field_labels = {
            "updated_at_start": _("Start updated date"),
            "updated_at_end": _("End updated date"),
            "created_at_start": _("Start created date"),
            "created_at_end": _("End updated date"),
        }
