from django_filters import rest_framework as filters
from blog.models import CommentModel


class CommentFilter(filters.FilterSet):
    """
    Реализует сортировки по дате создания в прямом и обратном порядке.
    """

    ordering = filters.OrderingFilter(
        fields=(("created_at", "created_at"),),
    )

    class Meta:
        model = CommentModel
        fields = ("ordering",)
