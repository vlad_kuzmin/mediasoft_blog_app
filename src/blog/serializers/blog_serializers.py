import itertools
from django.contrib.auth import get_user_model
from rest_framework import serializers
from blog.models import BlogModel, PostModel
from utils.serializers.fields import FilteredPrimaryKeyRelatedField
from .post_serializers import SimplePostSerializer
from users.serializers import SimpleUserSerializer

User = get_user_model()


class BlogSerializerMixin:
    """
    Примесь с логикой создания и обновления блогов.
    """

    def create(self, validated_data):
        """
        Метод создания нового блога.
        """

        current_user = self.context.get("request").user
        instance = BlogModel()

        instance.title = validated_data.pop("title", None)
        instance.description = validated_data.pop("description", None)

        # set value only for admins
        instance.owner = validated_data.pop("owner", current_user)
        instance.save()

        instance.authors.set(validated_data.pop("authors", None))

        return instance

    def update(self, instance, validated_data):
        """
        Метод обновления пользователя.
        """

        instance.title = validated_data.get("title", instance.title)
        instance.description = validated_data.get(
            "description", instance.description
        )

        # set value only for admins
        instance.owner = validated_data.get("owner", instance.owner)
        instance.save()

        instance.authors.set(
            validated_data.get("authors", instance.authors.all())
        )

        return instance


class SimpleBlogSerializer(BlogSerializerMixin, serializers.ModelSerializer):
    """
    Сериализует данные модели BlogModel.
    Используется для получения данных одного блога,
    обновления и создания нового блога.
    """

    def __init__(self, *args, **kwargs):
        """
        Динамически меняем поля только для чтения при инициализации класса
        в зависимости от статуса пользователя.
        """

        super(SimpleBlogSerializer, self).__init__(*args, **kwargs)

        user = self.context.get("request.user", {})
        initial_read_only_fields = getattr(self.Meta, "read_only_fields", ())

        user_read_only_fields = ("owner",)

        is_admin_user = getattr(user, "is_admin", False)
        is_superuser = getattr(user, "is_superuser", False)

        read_only_fields = set(initial_read_only_fields)

        if not is_admin_user or not is_superuser:
            read_only_fields = set(
                itertools.chain(
                    initial_read_only_fields, user_read_only_fields
                )
            )

        setattr(self.Meta, "read_only_fields", tuple(read_only_fields))

    authors = serializers.SlugRelatedField(
        many=True,
        slug_field="username",
        queryset=User.objects.all(),
    )

    owner = serializers.SlugRelatedField(
        read_only=True,
        slug_field="username",
    )

    posts = FilteredPrimaryKeyRelatedField(
        many=True,
        read_only=True,
        filter_kwargs={"status": "publish"},
        source="blog_posts",
    )

    class Meta:
        model = BlogModel
        fields = (
            "id",
            "title",
            "description",
            "created_at",
            "updated_at",
            "authors",
            "owner",
            "posts",
        )
        read_only_fields = (
            "id",
            "created_at",
            "updated_at",
        )


class DetailBlogSerializer(serializers.ModelSerializer):
    """
    Позволяет получить детализированные данные модели BlogModel.
    """

    authors = SimpleUserSerializer(many=True, read_only=True)

    owner = SimpleUserSerializer(read_only=True)

    posts = serializers.SerializerMethodField("get_posts")

    def get_posts(self, obj):
        filtered_posts = PostModel.objects.filter(blog=obj, status="publish")
        return SimplePostSerializer(filtered_posts, many=True).data

    class Meta:
        model = BlogModel
        fields = (
            "id",
            "title",
            "description",
            "created_at",
            "updated_at",
            "authors",
            "owner",
            "posts",
        )
        read_only_fields = (
            "id",
            "title",
            "description",
            "created_at",
            "updated_at",
            "authors",
            "owner",
            "posts",
        )
