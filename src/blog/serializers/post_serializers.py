import itertools
from django.contrib.auth import get_user_model
from django.db import transaction
from django.utils import timezone
from rest_framework import serializers
from blog.models import PostModel, BlogModel, TagModel
from blog.serializers import SimpleTagSerializer
from users.serializers import SimpleUserSerializer

User = get_user_model()


class PostSerializerMixin:
    """
    Примесь с логикой создания и обновления постов.
    """

    def create(self, validated_data):
        """
        Метод создания нового поста.
        """

        current_user = self.context.get("request").user
        instance = PostModel()

        instance.title = validated_data.pop("title", None)
        instance.body = validated_data.pop("body", None)
        instance.author = current_user
        instance.status = validated_data.pop("status", "draft")

        blog_id = self.context.get("blog_id", None)
        blog = BlogModel.objects.get(pk=blog_id) if blog_id else None

        # Если пост был опубликован обновляем дату обновления блога в соответствии с датой создания поста.
        if instance.is_published:
            instance.created_at = timezone.now()
            blog.updated_at = instance.created_at

        blog.save()

        instance.blog = blog
        instance.save()

        tags_data = validated_data.pop("tags", None)

        # Связываем объекты тегов с постом.
        if tags_data:
            for tag_data in tags_data:
                tag, _ = TagModel.objects.get_or_create(**tag_data)
                instance.tags.add(tag)

        return instance

    def update(self, instance, validated_data):
        """
        Метод обновления поста.
        """

        instance.title = validated_data.pop("title", instance.title)
        instance.body = validated_data.pop("body", instance.body)

        blog_id = self.context.get("blog_id", None)
        blog = BlogModel.objects.get(pk=blog_id) if blog_id else None
        status = validated_data.pop("status", instance.status)

        # Если пост был опубликован обновляем дату обновления блога в соответствии с датой создания поста.
        if instance.status == "draft" and status == "publish":
            instance.created_at = timezone.now()
            blog.updated_at = instance.created_at

        instance.status = status
        blog.save()

        instance.blog = blog
        instance.save()

        tags_data = validated_data.pop("tags", None)
        tags = []

        # Связываем объекты тегов с постом.
        if tags_data:
            for tag_data in tags_data:
                tag, _ = TagModel.objects.get_or_create(**tag_data)
                tags.append(tag)

        instance.tags.set(tags)

        return instance


class SimplePostSerializer(PostSerializerMixin, serializers.ModelSerializer):
    """
    Сериализует данные модели PostModel.
    Используется для получения данных одного поста,
    обновления и создания нового поста.
    """

    def __init__(self, *args, **kwargs):
        """
        Динамически меняем поля только для чтения при инициализации класса
        в зависимости от статуса пользователя.
        """

        super(SimplePostSerializer, self).__init__(*args, **kwargs)

        user = self.context.get("request.user", {})
        initial_read_only_fields = getattr(self.Meta, "read_only_fields", ())

        user_read_only_fields = ("likes", "views")

        is_admin_user = getattr(user, "is_admin", False)
        is_superuser = getattr(user, "is_superuser", False)

        read_only_fields = set(initial_read_only_fields)

        if not is_admin_user or not is_superuser:
            read_only_fields = set(
                itertools.chain(
                    initial_read_only_fields, user_read_only_fields
                )
            )

        setattr(self.Meta, "read_only_fields", tuple(read_only_fields))

    author = serializers.SlugRelatedField(
        slug_field="username",
        read_only=True,
    )

    tags = SimpleTagSerializer(
        many=True,
        required=False,
    )

    blog = serializers.PrimaryKeyRelatedField(
        read_only=True,
    )

    class Meta:
        model = PostModel
        fields = (
            "id",
            "author",
            "title",
            "body",
            "status",
            "is_published",
            "created_at",
            "likes",
            "views",
            "tags",
            "blog",
        )
        read_only_fields = (
            "id",
            "author",
            "is_published",
            "created_at",
        )


class DetailPostSerializer(serializers.ModelSerializer):
    """
    Позволяет получить детализированные данные модели PostModel.
    """

    author = SimpleUserSerializer(read_only=True)

    tags = SimpleTagSerializer(many=True, read_only=True)

    blog = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = PostModel
        fields = (
            "id",
            "author",
            "title",
            "body",
            "is_published",
            "created_at",
            "likes",
            "views",
            "tags",
            "blog",
        )
        read_only_fields = (
            "id",
            "author",
            "title",
            "body",
            "is_published",
            "created_at",
            "likes",
            "views",
            "tags",
            "blog",
        )


class PostLikeSerializer(serializers.ModelSerializer):
    """
    Сериализатор для инкрементации лайков поста.
    """

    def update(self, instance, validated_data):
        """
        Метод обновления лайков поста.
        """

        instance.likes += 1

        with transaction.atomic():
            instance.save(update_fields=("likes",))

        return instance

    class Meta:
        model = PostModel
        fields = (
            "id",
            "likes",
        )
        read_only_fields = (
            "id",
            "likes",
        )
