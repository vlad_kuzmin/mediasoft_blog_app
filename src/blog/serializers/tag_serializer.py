from rest_framework import serializers
from blog.models import TagModel


class SimpleTagSerializer(serializers.ModelSerializer):
    """
    Сериализует данные модели TagModel.
    Используется для получения данных одного тега,
    обновления и создания нового тега.
    """

    def create(self, validated_data):
        """
        Метод создания нового тега.
        """
        instance = TagModel()
        instance.name = validated_data.pop("name", None)
        instance.save()

        return instance

    def update(self, instance, validated_data):
        """
        Метод обновления тега.
        """
        instance.name = validated_data.pop("name", instance.name)
        instance.save()

        return instance

    class Meta:
        model = TagModel
        fields = (
            "id",
            "name",
        )
        read_only_fields = ("id",)
        extra_kwargs = {"name": {"validators": ()}}
