from rest_framework import serializers
from django.utils import timezone
from blog.models import CommentModel, PostModel


class CommentSerializerMixin:
    """
    Примесь с логикой создания и обновления комментария.
    """

    def create(self, validated_data):
        """
        Метод создания нового комментария.
        """
        current_user = self.context.get("request").user
        instance = CommentModel()

        instance.author = current_user
        instance.body = validated_data.pop("body", None)
        instance.created_at = timezone.now()

        post_id = self.context.get("post_id")
        post = PostModel.objects.get(pk=post_id) if post_id else None
        instance.post = post

        instance.save()

        return instance

    def update(self, instance, validated_data):
        """
        Метод обновления комментария.
        """
        instance.body = validated_data.pop("body", instance.body)
        instance.save(update_fields=("body",))

        return instance


class SimpleCommentSerializer(
    CommentSerializerMixin, serializers.ModelSerializer
):
    """
    Сериализует данные модели CommentModel.
    Используется для получения данных одного комментария,
    обновления и создания нового комментария.
    """

    author = serializers.SlugRelatedField(
        slug_field="username", read_only=True
    )
    post = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = CommentModel
        fields = (
            "id",
            "author",
            "post",
            "body",
            "created_at",
        )
        read_only_fields = (
            "id",
            "created_at",
            "author",
            "post",
        )
