from django.db import transaction
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema
from rest_framework.filters import SearchFilter
from rest_framework.mixins import UpdateModelMixin
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import (
    ListCreateAPIView,
    GenericAPIView,
)
from rest_framework.permissions import (
    IsAuthenticatedOrReadOnly,
    IsAuthenticated,
)
from rest_framework.response import Response
from blog.filters.post_filters import PostFilter
from blog.models import PostModel, BlogModel
from blog.serializers import (
    SimplePostSerializer,
    DetailPostSerializer,
    PostLikeSerializer,
)
from users.permissions import (
    IsAdminUser,
    IsAuthorOrReadOnly,
)
from utils.views.generics import RetrieveUpdateDestroyAPIView


class PostListCreateView(ListCreateAPIView):
    """
    Получить список постов / создать новый пост.
    """

    serializer_class = SimplePostSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    lookup_field = "pk"
    lookup_url_kwarg = "blog_id"
    filterset_class = PostFilter
    filter_backends = (DjangoFilterBackend, SearchFilter)
    search_fields = ("title", "author__username")

    def get_queryset(self):
        """
        Определяем queryset для просмотра списка постов.
        """
        blog_id = self.kwargs.get("blog_id", None)
        return PostModel.objects.exclude(status="draft").filter(
            blog__pk=blog_id
        )

    def get_serializer_context(self):
        """
        Передаем id блога в контекст сериализатора.
        """
        context = super().get_serializer_context()
        context.update({"blog_id": self.kwargs.get("blog_id", None)})

        return context

    def perform_create(self, serializer):
        """
        Проверяем права при публикации поста в блог.
        """
        blog_id = self.kwargs.get("blog_id", None)
        blog = BlogModel.objects.get(pk=blog_id)

        if bool(
            blog.owner != self.request.user
            and self.request.user not in blog.authors.all()
        ):
            raise PermissionDenied

        serializer.save()

    @extend_schema(
        summary="List posts view",
        description="Получить список постов",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @extend_schema(
        summary="Create posts view",
        description="Создать новый пост",
    )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class PostDetailUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    """
    Получить / изменить / удалить конкретный пост.
    """

    permission_classes = (IsAuthorOrReadOnly | IsAdminUser,)
    queryset = PostModel.objects.exclude(status="draft")
    lookup_field = "pk"
    lookup_url_kwarg = "post_id"

    def get_serializer_class(self):
        """
        На гет запросе используем сериализатор для детализации,
        на остальных простой сериализатор.
        """
        if self.request.method == "GET":
            return DetailPostSerializer
        return SimplePostSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        Инкриментируем счетчик просмотров при запросе поста.
        """
        instance = self.get_object()
        instance.views += 1

        with transaction.atomic():
            instance.save(update_fields=("views",))

        serializer = self.get_serializer(instance)

        return Response(serializer.data)

    @extend_schema(
        summary="Post detail view",
        description="Получить данные конкретного поста",
    )
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @extend_schema(
        summary="Post patch update view",
        description="Частично изменить данные конкретного поста",
    )
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @extend_schema(
        summary="Post delete view",
        description="Удалить конкретный пост по id",
    )
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class PostLikeView(UpdateModelMixin, GenericAPIView):
    """
    Представление инкрементации лайков поста.
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = PostLikeSerializer
    queryset = PostModel.objects.exclude(status="draft")
    lookup_field = "pk"
    lookup_url_kwarg = "post_id"

    @extend_schema(
        summary="Post like view",
        description="Лайкнуть пост",
    )
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
