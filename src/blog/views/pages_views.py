from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from blog.filters import BlogFilter
from blog.filters.post_filters import PostFilter
from blog.models import PostModel, BlogModel
from blog.serializers import SimplePostSerializer, SimpleBlogSerializer
from users.models import UserSubscriptionsModel
from users.serializers import SimpleUserSubscriptionsSerializer


class MainPageView(ListAPIView):
    """
    Позволяет получить последние n постов со всех блогов.
    Количество постов контролируется через параметр page_size.
    """

    serializer_class = SimplePostSerializer
    queryset = PostModel.objects.exclude(status="draft")
    filterset_class = PostFilter
    filter_backends = (DjangoFilterBackend, SearchFilter)
    search_fields = ("title", "author__username")

    @extend_schema(
        summary="Main page view",
        description="Получить последние n постов со всех блогов",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BlogsPageView(ListAPIView):
    """
    Позволяет получить последние n блогов по дате обновления.
    Количество блогов контролируется через параметр page_size.
    """

    serializer_class = SimpleBlogSerializer
    queryset = BlogModel.objects.all()
    filterset_class = BlogFilter
    filter_backends = (DjangoFilterBackend, SearchFilter)
    search_fields = ("title", "authors__username", "owner__username")

    @extend_schema(
        summary="Blogs page view",
        description="Получить последние n блогов по дате обновления",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BlogPostsPageView(ListAPIView):
    """
    Позволяет получить последние n постов конкретного блога.
    Дефолтная сортировка по дате публикации поста.
    Количество постов контролируется через параметр page_size.
    """

    serializer_class = SimplePostSerializer
    queryset = PostModel.objects.exclude(status="draft")
    filterset_class = PostFilter
    filter_backends = (DjangoFilterBackend, SearchFilter)
    search_fields = ("title", "author__username")
    lookup_url_kwarg = "blog_id"
    lookup_field = "blog__pk"

    @extend_schema(
        summary="Blog posts page view",
        description="Получить последние n постов блога по дате обновления",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class MyPostsPageView(ListAPIView):
    """
    Позволяет получить посты текущего пользователя.
    Дефолтная сортировка по дате публикации поста.
    Количество постов контролируется через параметр page_size.
    """

    serializer_class = SimplePostSerializer
    permission_classes = (IsAuthenticated,)
    filterset_class = PostFilter
    filter_backends = (DjangoFilterBackend, SearchFilter)
    search_fields = ("title", "author__username")

    def get_queryset(self):
        current_user = getattr(self.request, "user", None)
        queryset = PostModel.objects.exclude(status="draft").filter(
            author=current_user
        )

        return queryset

    @extend_schema(
        summary="My posts page view",
        description="Получить посты текущего пользователя",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class MySubscriptionsPageView(ListAPIView):
    """
    Получить список подписок текущего пользователя.
    """

    serializer_class = SimpleUserSubscriptionsSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        current_user = getattr(self.request, "user", None)
        queryset = UserSubscriptionsModel.objects.filter(user=current_user)

        return queryset

    @extend_schema(
        summary="My subscriptions page view",
        description="Получить список подписок текущего пользователя",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
