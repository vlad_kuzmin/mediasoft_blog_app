from .blog_api_views import BlogListCreateView, BlogDetailUpdateDestroyView
from .post_api_views import PostListCreateView, PostDetailUpdateDestroyView, PostLikeView
from .comment_api_views import CommentListCreateView, CommentDestroyView
from .tag_api_views import TagDestroyView
from .pages_views import MainPageView, BlogsPageView, BlogPostsPageView, MyPostsPageView, MySubscriptionsPageView
