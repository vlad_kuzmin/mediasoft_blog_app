from drf_spectacular.utils import extend_schema
from rest_framework.generics import ListCreateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from blog.filters.comment_filters import CommentFilter
from blog.models import CommentModel
from blog.serializers import SimpleCommentSerializer
from users.permissions import IsAdminUser, IsAuthor


class CommentListCreateView(ListCreateAPIView):
    """
    Получить список комментариев или создать новый.
    """

    serializer_class = SimpleCommentSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = CommentModel.objects.all()
    lookup_field = "pk"
    lookup_url_kwarg = "post_id"
    filterset_class = CommentFilter

    def get_serializer_context(self):
        """
        Передаем id поста в контекст сериализатора.
        """
        context = super().get_serializer_context()
        context.update({"post_id": self.kwargs.get("post_id", None)})

        return context

    @extend_schema(
        summary="List post comments view",
        description="Получить список комментариев к посту",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @extend_schema(
        summary="Create post comments view",
        description="Создать новый комментарий к посту",
    )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class CommentDestroyView(DestroyAPIView):
    """
    Удалить комментарий.
    """

    serializer_class = SimpleCommentSerializer
    permission_classes = (IsAdminUser | IsAuthor,)
    lookup_field = "pk"
    lookup_url_kwarg = "comment_id"
    queryset = CommentModel.objects.all()

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
