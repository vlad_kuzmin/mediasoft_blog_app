from drf_spectacular.utils import extend_schema
from rest_framework.generics import DestroyAPIView
from blog.models import TagModel
from users.permissions import IsAdminUser
from blog.serializers import SimpleTagSerializer


class TagDestroyView(DestroyAPIView):
    """
    Удалить тег.
    """

    serializer_class = SimpleTagSerializer
    permission_classes = (IsAdminUser,)
    lookup_field = "pk"
    lookup_url_kwarg = "tag_id"
    queryset = TagModel.objects.all()

    @extend_schema(
        summary="Tag delete view",
        description="Удалить тег",
    )
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
