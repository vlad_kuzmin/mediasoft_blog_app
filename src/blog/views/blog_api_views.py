from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema
from rest_framework.filters import SearchFilter
from rest_framework.generics import (
    ListCreateAPIView,
)
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from blog.models import BlogModel
from blog.serializers import (
    SimpleBlogSerializer,
    DetailBlogSerializer,
)
from users.permissions import IsAdminUser, IsOwnerOrReadOnly
from utils.views.generics import RetrieveUpdateDestroyAPIView
from blog.filters import BlogFilter


class BlogListCreateView(ListCreateAPIView):
    """
    Получить список блогов или создать новый.
    """

    serializer_class = SimpleBlogSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = BlogModel.objects.all()
    filterset_class = BlogFilter
    filter_backends = (DjangoFilterBackend, SearchFilter)
    search_fields = ("title", "authors__username", "owner__username")

    @extend_schema(
        summary="List blogs view",
        description="Получить список блогов",
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @extend_schema(
        summary="Create blog view",
        description="Создать новый блог",
    )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class BlogDetailUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    """
    Получить или изменить данные конкретного блога по id.
    """

    serializer_class = DetailBlogSerializer
    permission_classes = (IsOwnerOrReadOnly | IsAdminUser,)
    queryset = BlogModel.objects.all()
    lookup_field = "pk"
    lookup_url_kwarg = "blog_id"

    def get_serializer_class(self):
        if self.request.method == "GET":
            return DetailBlogSerializer
        return SimpleBlogSerializer

    @extend_schema(
        summary="Blog detail view",
        description="Получить данные конкретного блога",
    )
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @extend_schema(
        summary="Blog patch update view",
        description="Частично изменить данные конкретного блога",
    )
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @extend_schema(
        summary="User delete view",
        description="Удалить конкретный блог по id",
    )
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
