from django.urls import path
from blog.views import (
    MainPageView,
    BlogsPageView,
    BlogPostsPageView,
    MyPostsPageView,
    MySubscriptionsPageView,
)

urlpatterns = [
    # Главная страница (последние n постов со всех блогов).
    path(
        "main",
        MainPageView.as_view(),
        name="main_page_view",
    ),
    # Страница блогов (последние n блогов по дате обновления).
    path(
        "blogs",
        BlogsPageView.as_view(),
        name="blogs_page_view",
    ),
    # Страница постов конкретного блога (последние n постов по дате обновления).
    path(
        "blogs/<int:blog_id>/posts",
        BlogPostsPageView.as_view(),
        name="blog_posts_page_view",
    ),
    # Страница постов конкретного пользователя (последние n постов по дате обновления).
    path(
        "my_posts",
        MyPostsPageView.as_view(),
        name="my_posts_page_view",
    ),
    # Страница подписок конкретного пользователя.
    path(
        "my_subscriptions",
        MySubscriptionsPageView.as_view(),
        name="my_subscriptions_page_view",
    ),
]
