from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from blog.models import BlogModel

User = get_user_model()


class Command(BaseCommand):
    """
    Создаёт дефолтный блог если такового не существует.
    Используется при инициализации дефолтного блога проекта.
    """

    help = "Creates a default blog if one does not exist"

    def handle(self, *args, **options):
        try:
            owner = User.objects.get(username="blog_user")
        except User.DoesNotExist:
            raise User.DoesNotExist(
                "Create default users before default blog creation"
            )

        blog, created = BlogModel.objects.get_or_create(
            title="Mediasoft Blog",
            description="Best Mediasoft blog",
            owner=owner,
        )
        blog.authors.add(owner)

        if created is True:
            self.stdout.write(
                self.style.SUCCESS(
                    f"Default blog '{blog.title}' successfully created"
                ),
                ending="\n",
            )
        else:
            self.stdout.write(
                self.style.SUCCESS(
                    f"Default blog '{blog.title}' already created"
                ),
                ending="\n",
            )
