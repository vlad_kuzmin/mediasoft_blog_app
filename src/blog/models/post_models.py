from django.db import models
from django.utils.translation import gettext_lazy as _
from utils.model_helpers import default_post_user

POST_DRAFT_STATUS = "draft"
POST_PUBLISHED_STATUS = "publish"

POST_STATUSES = (
    (POST_DRAFT_STATUS, _("Draft")),
    (POST_PUBLISHED_STATUS, _("Published")),
)


class PostModel(models.Model):
    """
    Модель поста блога.
    Содержит внутренние, а также метаданные поста.
    """

    title = models.CharField(
        _("title"),
        max_length=255,
        help_text=_("title of post"),
    )

    body = models.TextField(
        _("body"),
        help_text=_("post content"),
    )

    status = models.CharField(
        _("post status"),
        max_length=10,
        choices=POST_STATUSES,
        default=POST_DRAFT_STATUS,
        help_text="status of post",
    )

    created_at = models.DateTimeField(
        _("created at"),
        blank=True,
        null=True,
        help_text=_("date the post was published"),
    )

    likes = models.IntegerField(
        _("likes"),
        default=0,
        help_text=_("post likes counter"),
    )

    views = models.IntegerField(
        _("views"),
        default=0,
        help_text=_("post views counter, updated when a post is opened"),
    )

    tags = models.ManyToManyField(
        "blog.TagModel",
        verbose_name=_("post tags"),
        related_name="tag_posts",
    )

    blog = models.ForeignKey(
        "blog.BlogModel",
        on_delete=models.CASCADE,
        verbose_name=_("blog"),
        help_text=_("blog this post refers to"),
        related_name="blog_posts",
    )

    author = models.ForeignKey(
        "users.User",
        on_delete=models.SET(default_post_user),
        verbose_name=_("author"),
        help_text=_("author of post"),
        related_name="author_posts",
    )

    def __str__(self):
        return f"{self.title} (author: {self.author.username})"

    @property
    def is_published(self) -> bool:
        return self.status == POST_PUBLISHED_STATUS

    class Meta:
        verbose_name = _("post")
        verbose_name_plural = _("posts")
        ordering = ["-created_at"]
