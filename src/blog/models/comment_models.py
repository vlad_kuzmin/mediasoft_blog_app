from django.db import models
from django.utils.translation import gettext_lazy as _
from utils.text_helpers import truncate_words


class CommentModel(models.Model):
    """
    Модель комментария относящегося к посту.
    Содержит внутренние, а также метаданные комментария.
    """

    body = models.TextField(
        _("body"),
        max_length=1000,
        help_text=_("body of comment"),
    )

    created_at = models.DateTimeField(
        _("created at"),
        auto_now_add=True,
        help_text=_("date of comment creation"),
    )

    post = models.ForeignKey(
        "PostModel",
        on_delete=models.CASCADE,
        verbose_name=_("post"),
        help_text=_("related post"),
        related_name="post_comments",
    )

    author = models.ForeignKey(
        "users.User",
        on_delete=models.SET_NULL,
        verbose_name=_("author"),
        blank=True,
        null=True,
        help_text=_("comment author"),
        related_name="author_comments",
    )

    def __str__(self):
        return f"{self.post.title} {truncate_words(self.body, 10)}"

    class Meta:
        verbose_name = _("comment")
        verbose_name_plural = _("comments")
        ordering = ["-created_at"]
