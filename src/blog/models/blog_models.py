from django.db import models
from django.utils.translation import gettext_lazy as _


class BlogModel(models.Model):
    """
    Модель поста блога.
    Содержит внутренние, а также метаданные блога.
    """

    title = models.CharField(
        _("title"),
        max_length=255,
        help_text=_("title of blog"),
    )

    description = models.CharField(
        _("description"),
        max_length=255,
        blank=True,
        null=True,
        help_text=_("blog description"),
    )

    created_at = models.DateTimeField(
        _("created at"),
        auto_now_add=True,
        help_text=_("date of blog creation"),
    )

    updated_at = models.DateTimeField(
        _("updated at"),
        blank=True,
        null=True,
        help_text=_("date of last blog update"),
    )

    authors = models.ManyToManyField(
        "users.User",
        verbose_name=_("authors"),
        help_text=_("authors of blog"),
        related_name="author_blogs",
    )

    owner = models.ForeignKey(
        "users.User",
        on_delete=models.CASCADE,
        verbose_name=_("owner"),
        help_text=_("blog owner"),
        related_name="owner_blogs",
    )

    def __str__(self):
        return f"{self.title} (owner: {self.owner.username})"

    class Meta:
        verbose_name = _("blog")
        verbose_name_plural = _("blogs")
        ordering = ("-updated_at",)
