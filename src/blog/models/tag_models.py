from django.db import models
from django.utils.translation import gettext_lazy as _


class TagModel(models.Model):
    """
    Модель тега.
    Содержит уникальное название тега.
    Используется для разграничения постов по 'категориям'.
    """

    name = models.CharField(
        _("name"), max_length=100, unique=True, help_text=_("name of tag")
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("tag")
        verbose_name_plural = _("tags")
        ordering = ("-name",)
