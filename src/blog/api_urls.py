from django.urls import path
from blog.views import (
    BlogListCreateView,
    BlogDetailUpdateDestroyView,
    PostListCreateView,
    PostDetailUpdateDestroyView,
    PostLikeView,
    CommentListCreateView,
    CommentDestroyView,
    TagDestroyView,
)

urlpatterns = [
    # Получить список блогов или создать новый в зависимости от типа запроса (GET/POST).
    path("", BlogListCreateView.as_view(), name="blog_list_create"),
    # Получить или изменить данные конкретного блога в зависимости от запроса (GET/PUT/PATCH).
    path(
        "<int:blog_id>",
        BlogDetailUpdateDestroyView.as_view(),
        name="blog_detail_update_delete",
    ),
    # Получить список постов блога или создать новый в зависимости от типа запроса (GET/POST).
    path(
        "<int:blog_id>/posts/",
        PostListCreateView.as_view(),
        name="post_list_create",
    ),
    # Получить или изменить данные конкретного поста в зависимости от запроса (GET/PUT/PATCH).
    path(
        "posts/<int:post_id>",
        PostDetailUpdateDestroyView.as_view(),
        name="post_detail_update_delete",
    ),
    # поставить лайк конкретному  посту посту.
    path(
        "posts/<int:post_id>/like",
        PostLikeView.as_view(),
        name="post_like",
    ),
    # Получить список комментариев к посту или создать новый в зависимости от типа запроса (GET/POST).
    path(
        "posts/<int:post_id>/comments",
        CommentListCreateView.as_view(),
        name="comment_list_create",
    ),
    # Удалить комментарий.
    path(
        "posts/comments/<int:comment_id>",
        CommentDestroyView.as_view(),
        name="comment_delete",
    ),
    # Удалить тег.
    path(
        "posts/tags/<int:tag_id>",
        TagDestroyView.as_view(),
        name="tag_delete",
    ),
]
